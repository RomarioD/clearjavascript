"use strict";

let addButton = document.querySelector('#add');
let result = document.querySelector('#result');
let viewCounterItems = document.querySelector('#count');
let counterItem = 0;

let transformArray = (arr) => {
    return arr.map((item) => {
        return {
            id: item.id,
            url: `http://${item.url}`,
            name: `${item.name.charAt(0).toUpperCase()}${item.name.slice(1).toLowerCase()}`,
            params: `${item.params.status} => ${item.params.progress}`,
            description: `${item.description.substring(0, 15)}...`,
            date: timeStamps(item.date)
        }
    });
};

let timeStamps = (timestamp) => {
    let tempTime = new Date(timestamp);
    let year = tempTime.getFullYear(),
        month = tempTime.getMonth() + 1,
        day = tempTime.getDate(),
        hours = tempTime.getHours(),
        minutes = tempTime.getMinutes();

    if (month < 10) {
        month = `0${month}`;
    }
    if (day < 10) {
        day = `0${day}`;
    }
    if (hours < 10) {
        hours = `0${hours}`;
    }
    if (minutes < 10) {
        minutes = `0${minutes}`;
    }

    return `${year}/${month}/${day} ${hours}:${minutes}`
};

let transformedArray = () => {
    return transformArray(data);
};

let createGalleryItem = (item) => {
    let itemId = `gallery-item-${item.id}`;
    let itemTemplate =
        `<div class="col-md-3 col-sm-4 col-xs-6 text-center" id="${itemId}" >\
            <div class="thumbnail">\
                <img src="${item.url}" alt="${item.name}">\
            </div>
            <div class="caption">\
                <h1>${item.name}</h1>\
                <p>${item.description}</p>\                    
                <p>${item.date}</p>\
            </div>\
            <button class="btn btn-danger" onclick="removeItem('${itemId}')">Удалить</button>
        </div>`;
    result.innerHTML += itemTemplate;
};

let removeItem = (itemId) => {
    let galleryItem = document.getElementById(itemId);
    result.removeChild(galleryItem);
    countedText(--counterItem);
    addButton.disabled = false;
};

let disableCounter = () => {
    alert("Больше нельзя добавить!");
    addButton.disabled = true;
};

let countedText = (count) => {
    viewCounterItems.textContent = count;
};

addButton.addEventListener('click', () => {
    addGalleryItem();
});

let addGalleryItem = () => {
    let itemArray = transformedArray()[counterItem];
    if (counterItem < 10) {
        createGalleryItem(itemArray);
        countedText(++counterItem);
    } else {
        disableCounter();
    }
};
