"use strict";

let firstLine = document.querySelector('#first-line');
let secondLine = document.querySelector('#second-line');
let thirdLine = document.querySelector('#third-line');

let transformArray = (arr) => {
    return arr.map((item) => {
        return {
            url: `http://${item.url}`,
            name: `${item.name.charAt(0).toUpperCase()}${item.name.slice(1).toLowerCase()}`,
            params: `${item.params.status} => ${item.params.progress}`,
            description: `${item.description.substring(0, 15)}...`,
            date: timeStamps(item.date)
        }
    });
};

let timeStamps = (timestamp) => {
    let tempTime = new Date(timestamp);
    let year = tempTime.getFullYear(),
        month = tempTime.getMonth() + 1,
        day = tempTime.getDate(),
        hours = tempTime.getHours(),
        minutes = tempTime.getMinutes();

    if (month < 10) {
        month = `0${month}`;
    }
    if (day < 10) {
        day = `0${day}`;
    }
    if (hours < 10) {
        hours = `0${hours}`;
    }
    if (minutes < 10) {
        minutes = `0${minutes}`;
    }

    return `${year}/${month}/${day} ${hours}:${minutes}`
};

let transformedArray = () => {
    return transformArray(data);
};

let createChildrenNode = (nodeName, nodeClass, nodeText = null, source = null, altImgName = null) => {
    let element = document.createElement(nodeName);
    element.className = nodeClass;
    element.textContent = nodeText;
    element.src = source;
    element.alt = altImgName;
    return element;
};

let appendChildren = (parent, children) => {
    for (let i = 0; i < children.length; i++) {
        parent.appendChild(children[i]);
    }
    return parent;
};

let createViaReplace = (itemsRange) => {    

    let itemTemplate = '<div class="col-sm-3 col-xs-6">\
                <img src="$url" alt="$name" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">$name</div>\
                    <div class="text-muted">$description</div>\
                    <div class="text-muted">$params</div>\
                    <div class="text-muted">$date</div>\
                </div>\
            </div>';

    for (let item of itemsRange) {
        let resultHTML = "";
        resultHTML = itemTemplate
            .replace(/\$name/gi, item.name)
            .replace("$url", item.url)
            .replace("$params", item.params)
            .replace("$description", item.description)
            .replace("$date", item.date);

        firstLine.innerHTML += resultHTML;
    }
};

let createViaInterpolation = (itemsRange) => {
    for (let item of itemsRange) {
        let itemTemplate = `<div class="col-sm-3 col-xs-6">\
                <img src="${item.url}" alt="${item.name}" class="img-thumbnail">\
                <div class="info-wrapper">\
                    <div class="text-muted">${item.name}</div>\
                    <div class="text-muted">${item.description}</div>\
                    <div class="text-muted">${item.params}</div>\
                    <div class="text-muted">${item.date}</div>\
                </div>\
            </div>`;
        secondLine.innerHTML += itemTemplate;
    }
};

let createViaCreateElement = (itemsRange) => {
    for (let item of itemsRange) {
        let itemTemplate = createChildrenNode("div", "col-sm-3 col-xs-6");
        let infoWrapper = createChildrenNode("div", "info-wrapper");
        let itemImage = createChildrenNode("img", "img-thumbnail", "", item.url, item.name);
        let infoWrapperContent = [createChildrenNode("div", "text-muted", item.name),
            createChildrenNode("div", "text-muted", item.description),
            createChildrenNode("div", "text-muted", item.params),
            createChildrenNode("div", "text-muted", item.date)];

        appendChildren(infoWrapper, infoWrapperContent);
        itemTemplate.appendChild(itemImage);
        itemTemplate.appendChild(infoWrapper);
        thirdLine.appendChild(itemTemplate);
    }
};

let createGallery = () => {
    let changedArray = transformedArray();
    createViaReplace(changedArray.slice(0, 3));
    createViaInterpolation(changedArray.slice(3, 6));
    createViaCreateElement(changedArray.slice(6, 9));
};

createGallery();
